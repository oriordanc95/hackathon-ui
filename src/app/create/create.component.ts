import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";
import { User } from '../shared/user';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  @Input() stockTradeDetails = { id: 0, price: 100, statusCode: 0, stockTicker: '',  volume: 0, side: '', createdTimestamp:0, user: new User}
  total: number = 0;

  tickers: any =[]

  ticker = this.actRoute.snapshot.params['ticker'];
  furtherInformation: any = [];

  constructor(
    public restApi: RestApiService,
    public router: Router,
    public actRoute: ActivatedRoute
  ) { }

  ngOnInit() {

    this.getTickerlist();

  }
  

  totalCalculation() {
    this.total = this.stockTradeDetails.price * this.stockTradeDetails.volume;
  }

  getTickerlist(){

    this.restApi.getTickerList().subscribe(res => {

      for (let entry of Object.entries(res)) {
        this.tickers.push(entry[1]['symbol'])
      }
      console.log(this.tickers) 

    })

  }

  findPrice() {
    let ticker = this.stockTradeDetails.stockTicker;
    this.restApi.findPrice(ticker).subscribe(res => {
      console.log(res);

      let x: any = [];
      for (let [key, value] of Object.entries(res)) {
        if (key == 'price_data') {

          x = value;

        }
      }

      console.log (x)

      let price = 0;
      this.stockTradeDetails.price = x[0]["value"]
      console.log(price);

    })

  }

  addShipper() {
    this.restApi.createStockTrade(this.stockTradeDetails).subscribe((data: {}) => {
      this.router.navigate(['/list'])
    })
  }




}



