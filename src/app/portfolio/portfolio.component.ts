import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {

  stockPortfolio: any = [];

  constructor(private restApi: RestApiService) { }

  ngOnInit(): void {
    this.loadPortfolio();
  }

  loadPortfolio() {
    this.restApi.getPortfolio()
    .subscribe(data => {
      this.stockPortfolio = data;
    })
  }

  
}
