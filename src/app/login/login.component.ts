import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { User } from '../shared/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input() loginDetails: User = { id: 0, lastName: '', firstname: '' };

  user: any;
  returnUrl: any;

  constructor(private actRoute: ActivatedRoute, private router: Router, private authService: AuthService) {
    if (this.authService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {
    this.user = {};
    this.returnUrl = this.actRoute.snapshot.queryParams['returnUrl'] || '/';
  }

  login() {
    this.authService.login(this.loginDetails)
    .subscribe(data => {
      if (data === null) {
        alert('Login failed - Try a different ID');
      } else {
        this.router.navigate(['/']);
      }
    });
  }

}
