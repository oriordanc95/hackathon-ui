import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute, Router } from '@angular/router';
import Chart from 'chart.js/auto'



@Component({
  selector: 'app-further-information',
  templateUrl: './further-information.component.html',
  styleUrls: ['./further-information.component.css']
})
export class FurtherInformationComponent implements OnInit {


  ticker = this.actRoute.snapshot.params['ticker'];


  furtherInformation: any = [];

  historicalInformation: any = {};


  chart: any = [];




  constructor(
    public restApi: RestApiService,
    public actRoute: ActivatedRoute,
    public router: Router

  ) { }

  ngOnInit(): void {
    this.getTradeAdvisor()
    this.getHistoricData()



    //this.chart = new Chart('canvas',{ type: 'line'})


  }

  getTradeAdvisor() {
    return this.restApi.getFurtherInformation(this.ticker).subscribe((data: {}) => {

      this.furtherInformation.push(data);
      // console.log(this.furtherInformation)
    })

  }

  getHistoricData() {
    return this.restApi.getHistoricalData(this.ticker).subscribe((data: {}) => {


      for (let [key, value] of Object.entries(data)) {
        if (key == 'price_data') {

          this.historicalInformation = value;

        }
      }

      console.log(this.historicalInformation)

      let name: any = [];
      let dates: any = [];
      let price: any = [];

      for (let entry of this.historicalInformation) {
        name.push(entry['name'])
        price.push(entry['value'])
      }

      





      for (let entry of name) {

        let jsdate = new Date(entry)
        dates.push(jsdate.toLocaleDateString('en',{year: 'numeric', month: 'short', day: 'numeric'}))

      }
      
      console.log(dates)
      console.log(price)



      this.chart = new Chart('canvas', {

        type: 'line',
        data: {
          labels: dates,
          datasets: [
            {
              data:  price,
              borderColor: '#3cba9f',
              fill: true
            }
          ]
        },
        options: {
          plugins: {
            legend:{
              display: false
            }
         },
          scales: {
            x: {
              title:{
                display: true,
                text: "Date" 
              },
              display: true
            },
            y: {
              title:{
                display: true,
                text: "price (USD)" 
              },
              display: true
            }
          }

        },

      })

    })



  }

}
