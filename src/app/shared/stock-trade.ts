import { User } from "./user";

export class StockTrade {
    constructor(){
        this.id=0;
        this.price=0;
        this.statusCode=0;
        this.stockTicker='';
        this.volume=0;
        this.side='';
        this.createdTimestamp= 0;
        this.user = new User;
    }

        id :number;
        price:number;
        statusCode:number;
        stockTicker:string;
        volume:number;
        side:string;
        createdTimestamp:number;
        user:User;
}
