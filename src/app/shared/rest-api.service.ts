import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StockTrade } from '../shared/stock-trade';
import { FurtherInformation } from '../shared/further-information';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  tradeUrl = environment.apiURL + 'stocktrade/';
  userUrl = environment.apiURL + 'user/';
  portfolioUrl = environment.apiURL + 'portfolio/';

  advisorURL = environment.advisorURL;
  historicCloseURL = environment.historicCloseURL;
  tickerListURL = environment.tickerListURL;

  user: User = new User;

  constructor(private http: HttpClient, private authService: AuthService) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  // HttpClient API get() method
  getStockTrades(): Observable<StockTrade> {
    this.getUser();
    return this.http.get<StockTrade>(this.tradeUrl + 'user/' + this.user.id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API get() method
  getStockTrade(id: any): Observable<StockTrade> {
    return this.http.get<StockTrade>(this.tradeUrl + id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API post() method
  createStockTrade(stocktrade: StockTrade): Observable<StockTrade> {
    this.getUser();
    stocktrade.user = this.user;
    return this.http.post<StockTrade>(this.tradeUrl + '', JSON.stringify(stocktrade), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API put() method
  updateStockTrade(id: number, stocktrade: StockTrade): Observable<StockTrade> {
    return this.http.put<StockTrade>(this.tradeUrl + '', JSON.stringify(stocktrade), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API delete() method
  deleteStockTrade(id: number) {
    return this.http.delete<StockTrade>(this.tradeUrl + id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getFurtherInformation(ticker: string): Observable<FurtherInformation> {
    return this.http.get<FurtherInformation>(this.advisorURL + ticker)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getHistoricalData(ticker: string): Observable<FurtherInformation> {

    return this.http.get<FurtherInformation>(this.historicCloseURL + ticker + '&num_days=30')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )

  }

  findPrice(ticker:string){

    return this.http.get<FurtherInformation>(this.historicCloseURL + ticker + '&num_days=1')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )

  }

  getTickerList(){
    return this.http.get<FurtherInformation>(this.tickerListURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )

  }

  getPortfolio() {
    this.getUser();
    return this.http.get<any>(this.portfolioUrl + this.user.id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // Error handling
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  getUser() {
    this.user = this.authService.currentUserValue;
  }
}


