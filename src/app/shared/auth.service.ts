import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUser: Observable<User>;
  private currentUserSubject: BehaviorSubject<any>;

  constructor(private http: HttpClient) {
    let userJson = sessionStorage.getItem('currentUser');
    this.currentUserSubject = userJson !== null ? new BehaviorSubject<any>(JSON.parse(userJson)) : new BehaviorSubject<any>(null);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue() {
    return this.currentUserSubject.value;
  }

  login(user: User) {
    return this.http.post<any>(environment.apiURL + 'user/login', user)
    .pipe(
      map(
        user => {
          sessionStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);

          if (user.error) {
            this.logout();
            return null;
          }

          return user;
        }
      )
    );
  }

  logout() {
    sessionStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
