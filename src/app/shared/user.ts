export class User {
    id: number;
    firstname: string;
    lastName: string;

    constructor() {
        this.id = 0;
        this.firstname = '';
        this.lastName = '';
    }
}
