import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-shipper-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  stockTrades: any = []; 
  

  constructor( 
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadStockTrades()
  }

  loadStockTrades() {
    return this.restApi.getStockTrades().subscribe((data: {}) => {
        this.stockTrades = data;
        console.log(this.stockTrades)
    })
  }

  deleteStockTrades(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteStockTrade(id).subscribe(data => {
        this.loadStockTrades()
      })
    }
  }  

}
