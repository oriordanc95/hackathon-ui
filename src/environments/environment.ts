// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  apiURL: 'http://unnamed-team-hackathon-unnamed-team-hackathon.emeadocker23.conygre.com/',
  advisorURL:'https://qz4sxjl623.execute-api.us-east-1.amazonaws.com/default/tradeAdvisor?ticker=',
  historicCloseURL: 'https://3p7zu95yg3.execute-api.us-east-1.amazonaws.com/default/priceFeed2?ticker=',
  tickerListURL: 'https://v588nmxc10.execute-api.us-east-1.amazonaws.com/default/tickerList'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
